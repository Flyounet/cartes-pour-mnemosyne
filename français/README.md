
# Cartes pour Mnemosyne : Français

Dans ce sous répertoire sont enregistrés les cartes pour le Français.

 * `conjugaison-groupes-verbaux.txt` : Ce fichier contient des cartes sur les groupes verbaux
     * Comment reconnaître les verbes du 1er,2ème,3ème groupe
     * Quels sont les groupes de certains verbes
 * `conjugaison-present.txt` : Ce fichier contient des cartes sur le _Présent de l'indicatif_
     * Conjugaison des verbes du 1er,2ème,3ème groupe et auxiliaires _être_ et _avoir_
 * `conjugaison-futur.txt` : Ce fichier contient des cartes sur le _Futur de l'indicatif_
     * Conjugaison des verbes du 1er,2ème,3ème groupe et auxiliaires _être_ et _avoir_
 * `conjugaison-imparfait.txt` : Ce fichier contient des cartes sur l'_Imparfait de l'indicatif_
     * Conjugaison des verbes du 1er,2ème,3ème groupe et auxiliaires _être_ et _avoir_
 

_N.B.:_ Les cartes, ici créées, nécessitent le plugin [Fast Format][ff] pour
[Mnemosyne][mnemo] pour la mise en page.



[mnemo]: http://mnemosyne-proj.org
[ff]: http://mnemosyne-proj.org/plugins/fast-format

[//]: vi:tw=85:cc=80:syntax=markdown

