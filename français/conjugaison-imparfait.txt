
Imparfait de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	*`étais`*<br><br>j'<b>étais</b>
Imparfait de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	*`étais`*<br><br>tu <b>étais</b>
Imparfait de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	*`était`*<br><br>il/elle/on <b>était</b>
Imparfait de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	*`étions`*<br><br>nous <b>étionss</b>
Imparfait de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	*`étiez`*<br><br>vous <b>étiez</b>
Imparfait de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	*`étaient`*<br><br>ils/elles <b>étaient</b>


Imparfait de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	*`avais`*<br><br>j'<b>avais</b>
Imparfait de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	*`avais`*<br><br>tu <b>avais</b>
Imparfait de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	*`avait`*<br><br>il/elle/on <b>avait</b>
Imparfait de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	*`avions`*<br><br>nous <b>avions</b>
Imparfait de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	*`aviez`*<br><br>vous <b>aviez</b>
Imparfait de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	*`avaient`*<br><br>ils/elles <b>avaient</b>


Imparfait de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`ais`*<br><br>j'aim<b>ais</b>
Imparfait de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`ais`*<br><br>tu aim<b>ais</b>
Imparfait de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`ait`*<br><br>il/elle/on aim<b>ait</b>
Imparfait de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`ions`*<br><br>nous aim<b>ions</b>
Imparfait de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`iez`*<br><br>vous aim<b>iez</b>
Imparfait de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`aient`*<br><br>ils/elles aim<b>aient</b>


Imparfait de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`ais`*<br><br>je fin<b>issais</b>
Imparfait de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`ais`*<br><br>tu fin<b>issais</b>
Imparfait de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`ait`*<br><br>il/elle/on fin<b>issait</b>
Imparfait de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`ions`*<br><br>nous fin<b>issions</b>
Imparfait de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`iez`*<br><br>vous fin<b>issiez</b>
Imparfait de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`aient`*<br><br>ils/elles fin<b>issaient</b>


Imparfait de l'indicatif<br>Verbe du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`ais`*<br><br>je mett<b>ais</b>
Imparfait de l'indicatif<br>Verbe du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`ais`*<br><br>tu mett<b>ais</b>
Imparfait de l'indicatif<br>Verbe du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`ait`*<br><br>il/elle/on mett<b>ait</b>
Imparfait de l'indicatif<br>Verbe du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`ions`*<br><br>nous mett<b>ions</b>
Imparfait de l'indicatif<br>Verbe du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`iez`*<br><br>vous mett<b>iez</b>
Imparfait de l'indicatif<br>Verbe du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`aient`*<br><br>ils/elles mett<b>aient</b>




