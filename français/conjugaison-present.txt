
Présent de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	*`suis`*<br><br>je <b>suis</b>
Présent de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	*`es`*<br><br>tu <b>es</b>
Présent de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	*`est`*<br><br>il/elle/on <b>est</b>
Présent de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	*`sommes`*<br><br>nous <b>sommes</b>
Présent de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	*`êtes`*<br><br>vous <b>êtes</b>
Présent de l'indicatif<br>Auxiliaire {être}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	*`sont`*<br><br>ils/elles <b>sont</b>


Présent de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	*`ai`*<br><br>j'<b>ai</b>
Présent de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	*`as`*<br><br>tu <b>as</b>
Présent de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	*`a`*<br><br>il/elle/on <b>a</b>
Présent de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	*`avons`*<br><br>nous <b>avons</b>
Présent de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	*`avez`*<br><br>vous <b>avez</b>
Présent de l'indicatif<br>Auxiliaire {avoir}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	*`ont`*<br><br>ils/elles <b>ont</b>


Présent de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`e`*<br><br>j'aim<b>e</b>
Présent de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`es`*<br><br>tu aim<b>es</b>
Présent de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`e`*<br><br>il/elle/on aim<b>e</b>
Présent de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`ons`*<br><br>nous aim<b>ons</b>
Présent de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`ez`*<br><br>vous aim<b>ez</b>
Présent de l'indicatif<br>Verbe du 1<sup>er</sup> groupe {ex. aimer}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`ent`*<br><br>ils/elles aim<b>ent</b>


Présent de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`is`*<br><br>je fin<b>is</b>
Présent de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`is`*<br><br>tu fin<b>is</b>
Présent de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`it`*<br><br>il/elle/on fin<b>it</b>
Présent de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`issons`*<br><br>nous fin<b>issons</b>
Présent de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`issez`*<br><br>vous fin<b>issez</b>
Présent de l'indicatif<br>Verbe du 2<sup>ème</sup> groupe {ex. finir}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`issent`*<br><br>ils/elles fin<b>issent</b>


Présent de l'indicatif<br>Verbe _générique_ du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`s`*<br><br>je me<b>ts</b>
Présent de l'indicatif<br>Verbe _générique_ du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`s`*<br><br>tu me<b>ts</b>
Présent de l'indicatif<br>Verbe _générique_ du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`t`*<br><br>il/elle/on me<b>t</b>
Présent de l'indicatif<br>Verbe _générique_ du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`ons`*<br><br>nous mett<b>ons</b>
Présent de l'indicatif<br>Verbe _générique_ du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`ez`*<br><br>vous mett<b>ez</b>
Présent de l'indicatif<br>Verbe _générique_ du 3<sup>ème</sup> groupe {ex. mettre}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`ent`*<br><br>ils/elles mett<b>ent</b>


Présent de l'indicatif<br>Verbe finissant en _-dre_ [sauf -oindre] du 3<sup>ème</sup> groupe {ex. prendre}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`ds`*<br><br>je pren<b>ds</b>
Présent de l'indicatif<br>Verbe finissant en _-dre_ [sauf -oindre] du 3<sup>ème</sup> groupe {ex. prendre}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`ds`*<br><br>tu pren<b>ds</b>
Présent de l'indicatif<br>Verbe finissant en _-dre_ [sauf -oindre] du 3<sup>ème</sup> groupe {ex. prendre}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`d`*<br><br>il/elle/on pren<b>d</b>
Présent de l'indicatif<br>Verbe finissant en _-dre_ [sauf -oindre] du 3<sup>ème</sup> groupe {ex. prendre}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`ons`*<br><br>nous pren<b>ons</b>
Présent de l'indicatif<br>Verbe finissant en _-dre_ [sauf -oindre] du 3<sup>ème</sup> groupe {ex. prendre}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`ez`*<br><br>vous pren<b>ez</b>
Présent de l'indicatif<br>Verbe finissant en _-dre_ [sauf -oindre] du 3<sup>ème</sup> groupe {ex. prendre}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`ent`*<br><br>ils/elles prenn<b>ent</b>


Présent de l'indicatif<br>Verbe finissant en _-oir_ du 3<sup>ème</sup> groupe {ex. vouloir}<br><br>Terminaison à la 1<sup>ère</sup> personne du singulier {j'/je}	-*`x`*<br><br>je veu<b>x</b>
Présent de l'indicatif<br>Verbe finissant en _-oir_ du 3<sup>ème</sup> groupe {ex. vouloir}<br><br>Terminaison à la 2<sup>ème</sup> personne du singulier {tu}	-*`x`*<br><br>tu veu<b>x</b>
Présent de l'indicatif<br>Verbe finissant en _-oir_ du 3<sup>ème</sup> groupe {ex. vouloir}<br><br>Terminaison à la 3<sup>ème</sup> personne du singulier {il/elle/on}	-*`t`*<br><br>il/elle/on veu<b>t</b>
Présent de l'indicatif<br>Verbe finissant en _-oir_ du 3<sup>ème</sup> groupe {ex. vouloir}<br><br>Terminaison à la 1<sup>ère</sup> personne du pluriel {nous}	-*`ons`*<br><br>nous voul<b>ons</b>
Présent de l'indicatif<br>Verbe finissant en _-oir_ du 3<sup>ème</sup> groupe {ex. vouloir}<br><br>Terminaison à la 2<sup>ème</sup> personne du pluriel {vous}	-*`ez`*<br><br>vous voul<b>ez</b>
Présent de l'indicatif<br>Verbe finissant en _-oir_ du 3<sup>ème</sup> groupe {ex. vouloir}<br><br>Terminaison à la 3<sup>ème</sup> personne du pluriel {ils/elles}	-*`ent`*<br><br>ils/elles veul<b>ent</b>


