
# Cartes pour Mnemosyne

[Mnémosyne][mnemo] est un outil affichant des cartes pour optimiser votre
apprentissage et votre mémoire sur le long terme.

## But

Les cartes, ici créées, sont initialement prévues pour des enfants de 8 à 9 ans,
étudiant en classe de CE2/CM1.

Les fichiers contenant des cartes sont écrits en `UTF-8` et au format **TSV**
(_tab-separated values_ : valeurs séparées par des tabulations).

_N.B.:_ Les cartes, ici créées, nécessitent le plugin [Fast Format][ff] pour
[Mnemosyne][mnemo] pour la mise en page.



[mnemo]: http://mnemosyne-proj.org
[ff]: http://mnemosyne-proj.org/plugins/fast-format

[//]: vi:tw=85:cc=80:syntax=markdown

