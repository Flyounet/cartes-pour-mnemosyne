
# Cartes pour Mnemosyne : Mathématiques

Dans ce sous répertoire sont enregistrés les cartes pour le Français.

 * `math-calcul-multiplication.txt` : Ce fichier contient des cartes sur les tables de multiplication
 * `math-calcul-addition.txt` : Ce fichier contient des cartes sur les tables d'addition
 * `math-numeration-chiffre-en-lettre.txt` : Ce fichier contient des cartes pour apprendre à écrire les chiffres en lettre
 * `math-geometrie-droites.txt` : Ce fichier contient des cartes pour sur les droites
 

_N.B.:_ Les cartes, ici créées, nécessitent le plugin [Fast Format][ff] pour
[Mnemosyne][mnemo] pour la mise en page.



[mnemo]: http://mnemosyne-proj.org
[ff]: http://mnemosyne-proj.org/plugins/fast-format

[//]: vi:tw=85:cc=80:syntax=markdown

