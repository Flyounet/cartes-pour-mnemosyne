
# Comment contribuer à ce projet

Les cartes, ici créées, sont initialement prévues pour des enfants de 8 à 9 ans,
étudiant en classe de CE2/CM1.

Les fichiers contenant des cartes sont écrits en `UTF-8` et au format **TSV**
(_tab-separated values_ : valeurs séparées par des tabulations).

Les règles restantes sont assez simples :

 * Les branches `dev` et `master` sont protégées.
    
    Cela signifie qu'il ne vous est pas possible de pousser vos _commits_, ou de
merger vos branches directement dans les deux branches sus-citées.
    
 * Vos branches doivent **impérativement** se nommer :
     * `feature-quelquechose` lorsque la branche parente est `dev`
     * `hotfix-quelquechose` lorsque la branche parente est `master`
 * Utilisez toujours les dernières versions disponibles : mettez à jour votre dépôt.
 * Pour la rédaction de cartes, proposez des fichiers texte au format **TSV**.
 * Le plugin [Fast Format][ff] est utlisé pour la rédaction de certaines cartes.
Pensez à regarder son fonctionnement.
 * Le _workflow_ suivant est obligatoire :
    * s'il s'agit d'une amélioration, il est vivement conseillé de d'abord ouvrir une
entrée de suivie dans l'outil _issue_ sur [Framagit][].
    * S'il s'agit d'un bug, il est **obligatoire** de commencé par ouvrir une entrée
de suivie dans l'outil _issue_ sur [Framagit][]. Si ce bug est déjà sur la branche
`master`, alors une branche `hotfix-*` (où `*` peut être l'identifiant de l'issue)
est nécessaire. Sinon, une branche `feature-issue*` doit être utilisée.
    * Effectuer une Merge Request (équivalent Pull Request sur Github), en
référençant l'issue corrigée.
 * Obtenez un remerciement pour votre travail.



[mnemo]: http://mnemosyne-proj.org
[ff]: http://mnemosyne-proj.org/plugins/fast-format
[Framagit]: https://framagit.org/Flyounet/cartes-pour-mnemosyne

[//]: vi:tw=85:cc=80:syntax=markdown

